package com.folcademy.miprimeraapi;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //INDICAMOS QUE ES UN REST CONTROLLER
public class HolaMundoController {
    //PODEMOS DEFINIR NUESTROS PRIMEROS END POINTS
    @RequestMapping("/") // SIRVE PARA T0DO (entre comillas la direccion)
    public String retornaCADENA()
    {
        return "Hola Mundo!";
    }
}
