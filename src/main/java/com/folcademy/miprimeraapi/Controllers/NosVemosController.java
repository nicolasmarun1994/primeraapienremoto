package com.folcademy.miprimeraapi.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NosVemosController {
    @RequestMapping("/nosvemos")
    public String retornaNosVemos()
    {
        return "Nos Vemos";
    }
}
